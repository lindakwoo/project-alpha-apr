from django.contrib import admin
from tasks.models import Task


@admin.register(Task)
class ReceiptAdmin(admin.ModelAdmin):
    list_display = [
        "id",
        "name",
        "start_date",
        "due_date",
        "is_completed",
        "assignee",
        "project_name",
    ]
