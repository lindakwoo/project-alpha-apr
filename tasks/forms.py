from django.forms import ModelForm
from django import forms
from .models import Task


class TaskForm(ModelForm):
    class Meta:
        model = Task
        fields = ["name", "start_date", "due_date", "project", "assignee"]

    def __init__(self, *args, **kwargs):
        super(TaskForm, self).__init__(*args, **kwargs)
        # Customize the 'list' field to use a dropdown
        self.fields["start_date"].widget = forms.DateInput(
            attrs={"type": "date"}
        )
        self.fields["due_date"].widget = forms.DateInput(
            attrs={"type": "date"}
        )
